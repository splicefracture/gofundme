(function(){
    _.mixin(_.str.exports());
    var shown = [];
    $("#search_box").bind("keyup",function(){
        var target = $(this).val().toLowerCase();
        
        shown = _.chain(menu_map)
        // match the input target to menu map
        .reduce(function(memo,item){
            if (!_.isEmpty(target) && _(item.name).startsWith(target)){
                memo.push("menu_view_"+item.id);
            }
            return memo;
        },[])
        // intercept the chain to show/hide
        .tap(function(to_show){
        
            var keep = _.intersection(to_show, shown);
            var hide = _.difference(shown, keep);
            var add  = _.difference(to_show, shown);
      
            _.each(hide,function(item){
                $("#"+item).hide();
            });
            _.each(add,function(item){
                $("#"+item).show();
            });
        })
        // convert wrap
        .value();
    });
})();
