<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout->setLayout('simplex');
    }

    public function indexAction()
    {

		$restaurants = RestaurantQuery::create()
                ->joinWith('Restaurant.Cuisine')
                ->orderById('desc')
                ->find();

        $map  = array();
        $items = array();

		foreach($restaurants as $row){
			$id = $row->getId();
			$cuisine_name = $row->getCuisine()->getName();
			$name = $row->getName();

            $items[]  = array( "id"=>$id,"name"=>$name, "cuisine"=>$cuisine_name );
            $map[] = array( "id"=>$id,"name"=>strtolower($name) );
            $map[] = array( "id"=>$id,"name"=>strtolower($cuisine_name) );
		}

        $this->view->menu_map   = $map;
        $this->view->menu_items = $items;
    }


}

